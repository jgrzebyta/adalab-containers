Docker containers for AdaLab RDFizer
====================================

## Description

This project brings together all Docker containers used by AdaLab project
data RDFizer. All containers are built in two main steps: compile/download binary tools and build the docker image.
Both steps are managed by [gradle](https://gradle.org/) scripts.

### Acknowledgements

The software development was supported by CHIST-ERA (Call 2013 -- AMCE), with funding from EPSRC (Grant no. [EP/M015661/1](http://gow.epsrc.ac.uk/NGBOViewGrant.aspx?GrantRef=EP/M015661/1); AdaLab).

The AdaLab project web page can be found at: http://www.adalab-project.org/.

## Containers

List of Docker containers and tools they contain:

* adalab-tools
    * [triple-loader](https://github.com/jgrzebyta/triple-loader)
    * [search-lucene](https://github.com/jgrzebyta/search-lucene)
* any23-container
    * [Apache ANY23](https://any23.apache.org/)

## Building

### Requirements

* Java 8 (SDK)
* [boot clj](http://boot-clj.com/)
* [Apache Maven](http://maven.apache.org/)
* [docker](https://www.docker.com/)

## Build

To build all containers it is enough to start gradle builder by command `./gradlew`
or `./gradlew.bat` on Windows machines.
